using Unity.Netcode;
using UnityEngine;
using TMPro;

public class GUIHealth : NetworkBehaviour
{
    public ushort latestDamage;
    private NetworkVariable<ushort> currentHP = new NetworkVariable<ushort>(10);
    public ushort maxHP;
    [SerializeField] private TextMeshProUGUI textGUI;

    private void Start() {
        textGUI.text = currentHP.Value + "/" + maxHP;
    }

    private void OnEnable() {
        currentHP.OnValueChanged += UpdateHPBar;
    }

    private void OnDisable() {
        currentHP.OnValueChanged -= UpdateHPBar;
    }

    public void GUICall() {
        if (maxHP > 0)
        {
            if (GUILayout.Button("Damage Player"))
            {
                // damages the player by 3 points
                ModifyHPServerRpc(3);
            }

            GUILayout.Label("HP: " + currentHP.Value + "/" + maxHP);
        }
    }

    [ServerRpc]
    public void ModifyHPServerRpc(ushort _damage) {
        if (currentHP.Value - _damage > 0)
            currentHP.Value -= _damage;
        else
            currentHP.Value = 0;

        // keeps track of the latest damage done
        latestDamage = (ushort)(_damage - currentHP.Value);

        //This will be printed on Server ONLY.
        //Debug.Log("Current HP is: " + currentHP.Value);
    }

    public void UpdateHPBar(ushort oldHP, ushort newHP) {
        if (oldHP != newHP)
        {
            // Update the Text in the World Space UI
            textGUI.text = newHP + "/" + maxHP;
        }
    }
}
