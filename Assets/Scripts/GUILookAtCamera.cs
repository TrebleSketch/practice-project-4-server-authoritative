using UnityEngine;

public class GUILookAtCamera : MonoBehaviour
{
    private Camera m_mainCamera;

    private void Start() {
        m_mainCamera = Camera.main;
    }

    private void LateUpdate() {
        transform.forward = Camera.main.transform.forward;

        //transform.LookAt(Camera.main.transform);
    }
}
