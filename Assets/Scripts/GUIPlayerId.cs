using Unity.Netcode;
using UnityEngine;
using TMPro;

public class GUIPlayerId : NetworkBehaviour
{

    [SerializeField] private TextMeshProUGUI text;
    //private NetworkVariable<ulong> clientID = new NetworkVariable<ulong>(999);

    //private void OnEnable() {
    //    clientID.OnValueChanged += UpdateGUIOnValueChanged;
    //}

    private void Start()
    {
        text.text = "id: " + GetComponent<NetworkObject>().OwnerClientId;


        //if (IsLocalPlayer)
        //    UpdateClientIdServerRpc();
        //else
        //    text.text = "id: " + clientID.Value;
    }

    //private void OnDisable() {
    //    clientID.OnValueChanged -= UpdateGUIOnValueChanged;
    //}



    //void UpdateGUIOnValueChanged(ulong oldID, ulong newID) {
    //    if (newID != oldID)
    //        text.text = "id: " + clientID.Value;
    //}

    //[ServerRpc]
    //void UpdateClientIdServerRpc() {
    //    Debug.Log("calling clientId: " + GetComponent<NetworkObject>().OwnerClientId);
    //    clientID.Value = GetComponent<NetworkObject>().OwnerClientId;
    //}
}
