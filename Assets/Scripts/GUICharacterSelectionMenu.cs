using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUICharacterSelectionMenu : MonoBehaviour
{
    private static GUICharacterSelectionMenu _instance;
    public static GUICharacterSelectionMenu Instance => _instance;

    // Have a system that is enabled when player has selected that they are ready.
    // Using Events or UnityEvents
    public delegate void PlayerReady();
    public static event PlayerReady OnPlayerReady;

    [SerializeField] private GameObject RightSelectionMenu;
    [SerializeField] private GameObject LeftSelectionMenu;

    // REWRITE CODE FOR SINGLE CLASS, CHARACTERS IN THE FUTURE

    [SerializeField] private GameObject m_catSelectArea;
    [SerializeField] private GameObject m_mouseSelectArea;
    private List<Toggle> m_charactersTogglesList = new List<Toggle>();
    private Dictionary<CatCharacterData, Toggle> catData = new Dictionary<CatCharacterData, Toggle>();
    private Dictionary<MouseCharacterData, Toggle> mouseData = new Dictionary<MouseCharacterData, Toggle>();
    private Toggle m_latestSelected;
    [SerializeField] private Button m_selectedWhenReadyButton;

    // FUTURE class for CharacterData storage
    private CharacterDataManager m_characterDataManager;
    
    public CatCharacterData CatSelectionData => m_catSelectedData;
    [SerializeField] private CatCharacterData m_catSelectedData;
    public MouseCharacterData MouseSelectionData => m_mouseSelectedData;
    [SerializeField] private MouseCharacterData m_mouseSelectedData;

    private void Awake() {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
        {
            Destroy(gameObject);
            Debug.LogWarning("GUICharacterSelectionMenu was just destroyed, because an extra instance was created. There can only be one");
        }
    }

    private void OnEnable() {
        OnPlayerReady += DisableSelectionGUI;
    }

    private void Start() {
        // Prepare data entry
        m_characterDataManager = CharacterDataManager.Instance;

        // Enable the menu if it ain't already
        LeftSelectionMenu.SetActive(true);
        RightSelectionMenu.SetActive(true);

        // WIP: will create the buttons at will in the future depending on the amount of character available

        // Obtain buttons data and attach the character data to it
        List<Toggle> catToggles = m_catSelectArea.GetComponentsInChildren<Toggle>().ToList();
        List<CatCharacterData> catCharData = m_characterDataManager.CatCharacterList;
        for (int i = 0; i < catToggles.Count; i++)
        {
            // this will check how many character data is available
            // enable the same amount of buttons as there is data to be interactable
            // save the button that is enabled to reference later

            if (i > catCharData.Count-1)
                continue;

            catToggles[i].interactable = true;
            catToggles[i].GetComponentInChildren<TMP_Text>().text = catCharData[i].Name;
            catData.Add(catCharData[i], catToggles[i]);
        }

        // Same as above, but for the mouse characters
        List<Toggle> mouseToggles = m_mouseSelectArea.GetComponentsInChildren<Toggle>().ToList();
        List<MouseCharacterData> mouseCharData = m_characterDataManager.MouseCharacterList;
        for (int i = 0; i < mouseToggles.Count; i++)
        {
            if (i > mouseCharData.Count-1)
                continue;

            mouseToggles[i].interactable = true;
            mouseToggles[i].GetComponentInChildren<TMP_Text>().text = mouseCharData[i].Name;
            mouseData.Add(mouseCharData[i], mouseToggles[i]);
        }

        m_charactersTogglesList = RightSelectionMenu.GetComponentsInChildren<Toggle>().ToList();

        // This ensures all of the selections are disabled if any one toggle is on during debugging
        foreach (Toggle check in m_charactersTogglesList)
        {
            // making sure they are ALL off
            if (check.isOn)
                check.isOn = false;

            // adding the listener to each button
            check.onValueChanged.AddListener(delegate { ChangeCharacterSelected(check); });
        }

        m_selectedWhenReadyButton.onClick.AddListener(delegate { PlayerReadyToPlay(); });
        m_selectedWhenReadyButton.gameObject.SetActive(false);
    }

    private void OnDisable() {
        OnPlayerReady -= DisableSelectionGUI;
    }

    private void ChangeCharacterSelected(Toggle toggleChanged) {
        // this is RUN on the button that was just turned on
        // buttons that was turned off won't run anything here
        if (toggleChanged.isOn)
        {
            // this sets the currently selected as the one that was JUST turned on
            m_latestSelected = toggleChanged;

            // Moves the Ready to play button next to the button that has been pressed
            m_selectedWhenReadyButton.transform.position = new Vector2(
                m_selectedWhenReadyButton.transform.position.x,
                toggleChanged.transform.position.y);
            if (!m_selectedWhenReadyButton.gameObject.activeInHierarchy)
                m_selectedWhenReadyButton.gameObject.SetActive(true);

            // forwards the character data to show what the player has picked "publically" in this script
            if (catData.ContainsValue(toggleChanged))
            {
                m_catSelectedData = catData.FirstOrDefault(data => data.Value == toggleChanged).Key;
                m_mouseSelectedData = null;
                //Debug.Log(m_catSelectedData.Name + " was just selected as player's character");
            }
            else if (mouseData.ContainsValue(toggleChanged))
            {
                m_catSelectedData = null;
                m_mouseSelectedData = mouseData.FirstOrDefault(data => data.Value == toggleChanged).Key;
                //Debug.Log(m_mouseSelectedData.Name + " was just selected as player's character");
            }
            else
            {
                m_catSelectedData = null;
                m_mouseSelectedData = null;
                //Debug.Log("Player selected to be a spectator.");
            }

            // Checks for the previously selected button to turn off
            foreach (var check in m_charactersTogglesList)
            {
                if (check != m_latestSelected && check.isOn)
                {
                    check.isOn = false;

                    //if (catData.ContainsValue(check))
                    //    Debug.Log("No longer selecting " + catData.FirstOrDefault(data => data.Value == check).Key.Name + " as character");
                    //else if (mouseData.ContainsValue(check))
                    //    Debug.Log("No longer selecting " + mouseData.FirstOrDefault(data => data.Value == check).Key.Name + " as character");
                    //else
                    //    Debug.Log("No longer selected to spectate.");
                }
            }
        }
        // this handles if players unselect what they have currently selected
        else
        {
            // All logic except for self-unselect is above, so return
            if (m_latestSelected != toggleChanged)
                return;

            // Because nothing is selected, so will disable the button that'll ready the game
            m_selectedWhenReadyButton.gameObject.SetActive(false);

            // Checks for what is the current button being turned off
            // Then makes sure that the data is null
            if (catData.ContainsValue(toggleChanged))
            {
                //Debug.Log("No longer selecting " + m_catSelectedData.Name + " as character");
                m_catSelectedData = null;
            }
            else if (mouseData.ContainsValue(toggleChanged))
            {
                //Debug.Log("No longer selecting " + m_mouseSelectedData.Name + " as character");
                m_mouseSelectedData = null;
            }
            //else
            //    Debug.Log("No longer selected to spectate.");
        }
    }

    private void PlayerReadyToPlay() {
        m_selectedWhenReadyButton.interactable = false;
        OnPlayerReady();
    }

    private void DisableSelectionGUI() {
        LeftSelectionMenu.SetActive(false);
        RightSelectionMenu.SetActive(false);
    }

    public bool IsInSpectatorMode() {
        if (CatSelectionData != null || MouseSelectionData != null)
            Debug.Log("GUICharacterSelectionMenu: Not in Spectator Mode, has a character selected");
        return CatSelectionData == null && MouseSelectionData == null;
    }

    public bool IsCatCharacterSelected() {
        if (CatSelectionData == null && MouseSelectionData == null)
            Debug.Log("GUICharacterSelectionMenu: Neither character selected, is in Spectator Mode");
        return CatSelectionData != null;
    }
}
