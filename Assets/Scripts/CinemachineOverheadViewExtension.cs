﻿using Cinemachine;
using System.Linq;
using UnityEngine;

public class CinemachineOverheadViewExtension : CinemachineExtension
{
    [SerializeField] private Camera m_orthoCamera;
    private PlayerControls m_playerControls;
    private PlayerMovement m_playerMovement;

    protected override void Awake() {
        base.Awake();
        m_playerControls = GetComponentInParent<PlayerControls>();
        m_playerMovement = GetComponentInParent<PlayerMovement>();
    }

    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime) {
#if UNITY_EDITOR
        // SCREAMS, only way to stop it from bugging out lol
        if (!Application.isPlaying)
            return;
#endif
        if (!m_playerMovement.IsLocalPlayer ||
            m_playerControls.CurrentControlState != PlayerControls.ControlStates.OverheadView)
        {
            Debug.Log("Overhead View Control is disabled");
            VirtualCamera.Priority = 0;
            m_orthoCamera.enabled = false;
            m_orthoCamera.GetComponent<AudioListener>().enabled = false;
            VirtualCamera.enabled = false;
            enabled = false;
            return;
        }

        if (vcam.Follow)
        {
            if (stage == CinemachineCore.Stage.Aim)
            {
                // Movement will be controlled by PlayerMovement
                // Zooming will be handled by this camera
                GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = m_playerMovement.CameraZoom;
            }
        }
    }

    // FUTURE, follow tutorials on Trello page for smooth single camera transitions between overhead and first-person views

    public void EnableCamera() {
        // should ensure it ALWAYS faces up/north towards the z direction
        transform.Rotate(new Vector3(0f, 0f, transform.rotation.eulerAngles.y));

        // ENSURES there are no other AudioListeners in the scene first
        foreach (var listener in FindObjectsOfType<AudioListener>())
        {
            if (listener.enabled)
                listener.enabled = false;
        }
        m_orthoCamera.GetComponent<AudioListener>().enabled = true;
        m_orthoCamera.enabled = true;

        //GetComponent<CinemachineVirtualCamera>().m_Lens.Dutch = transform.rotation.eulerAngles.y;

        VirtualCamera.enabled = true;
        enabled = true;

        VirtualCamera.Priority = 50;
    }
}