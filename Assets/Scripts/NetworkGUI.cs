using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Netcode;

public class NetworkGUI : MonoBehaviour
{
    public static NetworkGUI Instance => instance;
    private static NetworkGUI instance;
    ulong localClientId => NetworkManager.Singleton.LocalClientId;

    private void Awake() {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void OnGUI() {
        GUILayout.BeginArea(new Rect(10, 10, 300, 400));
        if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
        {
            StartButtons();
        }
        else
        {
            StatusLabels();
        }

        if (NetworkManager.Singleton.IsServer)
        {
            if (GUILayout.Button("Change Scene"))
            {
                string sceneName = SceneManager.GetActiveScene().buildIndex != 0 ? "TestScene" : "TestSceneNew";
                // Load Scene in via network
                NetworkManager.Singleton.SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
            }
            if (GUILayout.Button("Shutdown Server"))
            {
                NetworkManager.Singleton.Shutdown();
            }
        }

        if (NetworkManager.Singleton.IsClient)
        {
            if (GUILayout.Button("Exit Server/Quit Game"))
            {
                Application.Quit();
            }
            //if (NetworkManager.Singleton.LocalClient == null)
            //    Debug.Log("LocalClient is still null");
            //if (NetworkManager.Singleton.LocalClient.PlayerObject.TryGetComponent(out GUIHealth guiHealth))
            //{
            //    GUILayout.Label("localClientId: " + localClientId);
            //    guiHealth.GUICall();
            //}

            if (playerControls == null)
            {
                foreach (var player in FindObjectsOfType<PlayerControls>())
                {
                    if (player.IsLocalPlayer)
                        playerControls = player;
                }
            }

            GUILayout.Label("Control State: " + playerControls.CurrentControlState);
            playerControls.GUICall();
        }

        GUILayout.EndArea();
    }

    PlayerControls playerControls;

    static void StartButtons() {
        if (GUILayout.Button("Host")) NetworkManager.Singleton.StartHost();
        if (GUILayout.Button("Client")) NetworkManager.Singleton.StartClient();
        if (GUILayout.Button("Server")) NetworkManager.Singleton.StartServer();
    }

    static void StatusLabels() {
        var mode = NetworkManager.Singleton.IsHost ?
            "Host" : NetworkManager.Singleton.IsServer ? "Server" : "Client";

        GUILayout.Label("Transport: " +
            NetworkManager.Singleton.NetworkConfig.NetworkTransport.GetType().Name);
        GUILayout.Label("Mode: " + mode);
    }
}
