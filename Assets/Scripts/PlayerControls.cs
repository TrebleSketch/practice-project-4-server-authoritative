using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Netcode;

public class PlayerControls : NetworkBehaviour
{
    public enum ControlStates
    {
        OverheadView,
        FirstPersonView,
        ThirdPersonView,
        SPECTATING_MapView,
        SPECTATING_OverheadView,
        SPECTATING_FirstPersonView,
        SPECTATING_ThirdPersonView
    }

    private PlayerMovement m_playerMovement;
    private InputActions m_inputActions;
    [SerializeField] private ControlStates m_currentControlState = ControlStates.OverheadView;

    public ControlStates CurrentControlState => m_currentControlState;
    public Vector2 MovementVector => m_inputActions.Player.Move.ReadValue<Vector2>();
    public Vector2 LookVector => m_inputActions.Player.Look.ReadValue<Vector2>();
    public Vector2 ZoomVector => m_inputActions.Player.Zoom.ReadValue<Vector2>();
    // ZOOM VECTOR.
    // Each movement is 120
    // x = Middle Mouse (-tive is left, +tive is right)
    // y = Scroll Wheel (-tive is down, +tive is up)
    [Range(0.4f, 1f)]
    public float ZoomSensitivity = 0.5f;

    private List<CinemachineExtension> m_cinemachineExtensions;
    private CinemachinePOVExtension m_cinemachinePOVExtension;
    private CinemachineOverheadViewExtension m_cinemachineOverheadViewExtension;

    private void Awake() {
        m_playerMovement = GetComponent<PlayerMovement>();
        m_inputActions = new InputActions();
        m_cinemachineExtensions = GetComponentsInChildren<CinemachineExtension>(true).ToList();
    }

    private void OnEnable() {
        m_inputActions.Enable();
    }

    private void Start() {
        ulong localClientId = GetComponent<NetworkObject>().OwnerClientId;
        name = "Player " + localClientId;
    }

    private void Update() {
        if (!IsLocalPlayer)
            return;

        // Spectating Views - Allowed movements
        // ONLY ON DEATH (or waiting to join the game)
        //
        // Map View, able to rotate freely around fixed point
        // Overhead View, unable to move (No camera control)
        // First Person View, unable to move but follows player's view
        // Third Person View, able to rotate freely around player
        if (m_currentControlState == ControlStates.SPECTATING_MapView ||
            m_currentControlState == ControlStates.SPECTATING_OverheadView ||
            m_currentControlState == ControlStates.SPECTATING_FirstPersonView ||
            m_currentControlState == ControlStates.SPECTATING_ThirdPersonView)
        {
            Debug.Log("In Spectating mode");
            return;
        }

        // Overhead View - Movement Controls
        if (m_currentControlState == ControlStates.OverheadView)
        {
            // ADD PlayerMovement Function that allows the player to translate across (without look + jump detection + gravity)

            // Add networking in the future
            if (ZoomVector != Vector2.zero)
                m_playerMovement.ZoomCamera(ZoomVector, ZoomSensitivity);
                //Debug.Log("Overhead view, scroll: " + ZoomVector);
            return;
        }

        // First Person/Third Person - Movement Controls

        bool didJumpThisFrame = false;

        // Check if player has jumped this frame
        if (m_inputActions.Player.Jump.triggered)
            didJumpThisFrame = true;

        // Moves the local player while waiting for server's reply
        if (!IsHost)
            m_playerMovement.ClientMovementPrediction(MovementVector, LookVector, didJumpThisFrame, m_currentControlState);

        //Sends the server player's movement vector
        m_playerMovement.PlayerMovementUpdateServerRpc(MovementVector.x, MovementVector.y, LookVector.x, LookVector.y, didJumpThisFrame, m_currentControlState);
    }

    private void OnDisable() {
        m_inputActions.Disable();
    }

    public void GUICall() {
        if (GUILayout.Button("Enable Overhead View"))
        {
            m_currentControlState = ControlStates.OverheadView;
            FindObjectOfType<CinemachineOverheadViewExtension>().EnableCamera();
        }

        if (GUILayout.Button("Enable First Person View"))
        {
            m_currentControlState = ControlStates.FirstPersonView;
            FindObjectOfType<CinemachinePOVExtension>().EnableCamera();
        }
    }
}
