using Cinemachine;
using Unity.Netcode;
using UnityEngine;

public class CinemachinePOVExtension : CinemachineExtension
{
    private PlayerControls m_playerControls;
    private PlayerMovement m_playerMovement;
    private Vector3 m_startingRotation;

    protected override void Awake() {
        base.Awake();
        m_playerControls = GetComponentInParent<PlayerControls>();
        m_playerMovement = GetComponentInParent<PlayerMovement>();
    }

    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime) {
#if UNITY_EDITOR
        // SCREAMS, only way to stop it from bugging out lol
        if (!Application.isPlaying)
            return;
#endif
        // Future, spectating POV camera control code can be added here in the future
        if (!m_playerMovement.IsLocalPlayer ||
            (m_playerControls.CurrentControlState != PlayerControls.ControlStates.FirstPersonView &&
            m_playerControls.CurrentControlState != PlayerControls.ControlStates.ThirdPersonView))
        {
            Debug.Log("Camera POV Control is disabled");
            VirtualCamera.Priority = 0;
            VirtualCamera.enabled = false;
            enabled = false;
            return;
        }

        if (vcam.Follow)
        {
            if (stage == CinemachineCore.Stage.Aim)
            {
                if (m_startingRotation == null)
                    m_startingRotation = transform.localRotation.eulerAngles;
                m_startingRotation.y = m_playerMovement.GetVerticalCameraAngle();
                state.RawOrientation = Quaternion.Euler(m_startingRotation.y, m_playerMovement.transform.rotation.eulerAngles.y, 0f);
            }
        }
    }

    public void EnableCamera() {
        VirtualCamera.enabled = true;
        enabled = true;

        VirtualCamera.Priority = 50;
    }
}
