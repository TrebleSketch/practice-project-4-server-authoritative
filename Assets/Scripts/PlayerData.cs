using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerData : NetworkBehaviour
{
    public ushort CharacterSelectedUUID => _uuid;
    [SerializeField] private ushort _uuid;
    public CharacterDataBase.CharacterTypes PlayerCharacterMode { get; private set; }

    public CatCharacterData CatCharacterData => m_catCharacterData;
    [SerializeField] private CatCharacterData m_catCharacterData;
    public MouseCharacterData MouseCharacterData => m_mouseCharacterData;
    [SerializeField] private MouseCharacterData m_mouseCharacterData;

    private GUICharacterSelectionMenu m_characterSelectionMenu;

    private Transform m_modelSpawnGO;

    // When loading into the game as the player
    // Make sure script is ready to take the Chaarcter Data that is chosen.
    // Subscribe to OnPlayerReady in GUICharacterSelectionMenu

    // Once data is chosen, load in the selected data into the variable data above
    // Also load in the player models to move around with.

    // This script might need to be Networked???

    private void Start() {

        m_characterSelectionMenu = GUICharacterSelectionMenu.Instance;
        m_modelSpawnGO = transform.Find("_ModelGO");

        if (!IsLocalPlayer)
            return;
        GUICharacterSelectionMenu.OnPlayerReady += ObtainingCharacterData;
    }

    private void OnDisable() {
        if (!IsLocalPlayer)
            return;
        GUICharacterSelectionMenu.OnPlayerReady -= ObtainingCharacterData;
    }

    // Layer 9  - Spectator
    // Layer 10 - Cat
    // Layer 11 - Mouse
    private void ObtainingCharacterData() {
        CatCharacterData catData = m_characterSelectionMenu.CatSelectionData;
        MouseCharacterData mouseData = m_characterSelectionMenu.MouseSelectionData;

        if (catData == null && mouseData == null)
        {
            // this means player is in spectator mode
            Debug.Log("PlayerData: Player has entered spectator mode");
            gameObject.layer = 9;
            return;
        }
        else if (catData != null)
        {
            m_catCharacterData = catData;
            _uuid = catData.UUID;
            gameObject.layer = 10;
        }
        else if (mouseData != null)
        {
            m_mouseCharacterData = mouseData;
            _uuid = mouseData.UUID;
            gameObject.layer = 11;
        }
        SpawnCharacterModel(catData, mouseData);
    }

    private void SpawnCharacterModel(CatCharacterData catData, MouseCharacterData mouseData) {
        GameObject characterModel = null;

        if (catData != null)
        {
            characterModel = catData.CharacterNetworkedModel;
        }
        else if (mouseData != null)
        {
            characterModel = mouseData.CharacterNetworkedModel;
        }

        Instantiate(characterModel, m_modelSpawnGO);
    }
}
