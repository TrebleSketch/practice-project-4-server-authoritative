using System.Collections.Generic;
using System;
using UnityEngine;
using Unity.Netcode;

public class PlayerMovement : NetworkBehaviour
{
    [Serializable]
    public struct PositionSync
    {
        public double Time;
        public PlayerControls.ControlStates CurrentControlState;
        public bool IsGrounded;
        public Vector3 Position;
        public Vector2 DirectionVector; // x, being horizontal rotation (side-to-side). y, being vertical rotation (up-and-down)
        public Vector3 CharacterVelocity;
    }

    public NetworkVariable<bool> HasGravity = new NetworkVariable<bool>(true);
    public bool IsGrounded = true;

    [Header("Movement Vectors")]
    public Vector3 CharacterVelocity;
    public Vector3 CharacterVector;
    public Vector3 TargetVector;

    [Header("Curves")]
    [SerializeField] private AnimationCurve m_accelerationCurve;
    [SerializeField] private AnimationCurve m_decelerationCurve;

    [Header("Movement Settings")]
    [Range(0, 0.5f)]
    [SerializeField] private float m_lookSensitivity = 0.15f; // Link this with settings in the future
    [Range(2f, 8f)]
    [SerializeField] private float m_maxSpeedOnGround = 5f;
    [Range(4f, 10f)]
    [SerializeField] private float m_maxSpeedInAir = 6.5f;
    [Range(5f, 15f)]
    [SerializeField] private float m_jumpForce = 8f;
    // Sharpness for the movement when grounded.
    // A low value will make the player accelerate and decelerate slowly,
    // A high value will do the opposite.
    [Tooltip("Sharpness for the movement when grounded, lower values will slow down player acceleration/deceleration")]
    [SerializeField] private float m_movementSharpnessOnGround = 7f;
    [Tooltip("Physics layers to check for a Player Grounded Check")]
    public LayerMask GroundCheckLayers = -1;

    private float m_gravityY => -Physics.gravity.y;
    private Vector3 m_GroundNormal;
    private float m_lastTimeJumped = 0f;
    private float m_groundCheckDistance = 0.5f;

    private float m_cameraVerticalAngle = 0f;
    [SerializeField] private float m_cameraClampAngle = 89f;
    public float CameraZoom => m_cameraZoom;
    private float m_cameraZoom = 5f; // this is the "size" in Orthographic camera view
    private float m_minCameraZoom = 1f;
    private float m_maxCameraZoom = 30f;

    // Game waits for 0.2 seconds before checking for if player is grounded or not
    const float k_JumpGroundingPreventionTime = 0.2f;
    // Game checks if player is above this height in the air before checking if player is gruonded
    const float k_GroundCheckDistanceInAir = 0.08f;

    private CharacterController m_characterController;
    public List<PositionSync> m_previousPositions = new List<PositionSync>();
    private PositionSync m_syncThisFrame;

    private void Awake() {
        m_characterController = GetComponent<CharacterController>();
    }

    // Simulation grounded check, run on BOTH Server and Client.
    private bool GroundedCheck() {
        // Make sure that the ground check distance while already in air is very small, to prevent suddenly snapping to ground
        float chosenGroundCheckDistance =
            IsGrounded ? (m_characterController.skinWidth + m_groundCheckDistance) : k_GroundCheckDistanceInAir;

        // reset values before the ground check
        IsGrounded = false;
        m_GroundNormal = Vector3.up;

        // only try to detect ground if it's been a short amount of time since last jump; otherwise we may snap to the ground instantly after we try jumping
        if (Time.time >= m_lastTimeJumped + k_JumpGroundingPreventionTime)
        {
            // if we're grounded, collect info about the ground normal with a downward capsule cast representing our character capsule
            if (Physics.CapsuleCast(GetCapsuleBottomHemisphere(), GetCapsuleTopHemisphere(m_characterController.height),
                m_characterController.radius, Vector3.down, out RaycastHit hit, chosenGroundCheckDistance, GroundCheckLayers,
                QueryTriggerInteraction.Ignore))
            {
                // storing the upward direction for the surface found
                m_GroundNormal = hit.normal;

                // Only consider this a valid ground hit if the ground normal goes in the same direction as the character up
                // and if the slope angle is lower than the character controller's limit
                if (Vector3.Dot(hit.normal, transform.up) > 0f &&
                    IsNormalUnderSlopeLimit(m_GroundNormal))
                {
                    IsGrounded = true;

                    // handle snapping to the ground
                    if (hit.distance > m_characterController.skinWidth)
                    {
                        m_characterController.Move(Vector3.down * hit.distance);
                        // MOVE down to the ground
                    }
                }
            }
        }
        return IsGrounded;
    }

    // Gets the center point of the bottom hemisphere of the character controller capsule    
    Vector3 GetCapsuleBottomHemisphere() {
        return transform.position + (transform.up * m_characterController.radius);
    }

    // Gets the center point of the top hemisphere of the character controller capsule    
    Vector3 GetCapsuleTopHemisphere(float atHeight) {
        return transform.position + (transform.up * (atHeight - m_characterController.radius));
    }

    // Returns true if the slope angle represented by the given normal is under the slope angle limit of the character controller
    bool IsNormalUnderSlopeLimit(Vector3 normal) {
        return Vector3.Angle(transform.up, normal) <= m_characterController.slopeLimit;
    }

    // WIP, test function
    // not indicative of the final working function
    public void ZoomCamera(Vector2 zoomVector, float zoomSensitivity) {
        // y is scroll wheel
        m_cameraZoom = Mathf.Clamp(m_cameraZoom - (zoomVector.y * zoomSensitivity * Time.deltaTime), m_minCameraZoom, m_maxCameraZoom);
    }







    // This function TAKES IN ALL LOOK/MOVE VECTORS and JUMP data then sends it to the server to be processed
    [ServerRpc]
    public void PlayerMovementUpdateServerRpc(
        float moveVector_x, float moveVector_y,
        float lookVector_x, float lookVector_y,
        bool didJumpThisFrame,
        PlayerControls.ControlStates currentControlState)
    {
        m_syncThisFrame = new PositionSync();
        m_syncThisFrame.Time = NetworkManager.ServerTime.Time;
        m_syncThisFrame.CurrentControlState = currentControlState;

        // Calculate if grounded
        m_syncThisFrame.IsGrounded = GroundedCheck();

        // First, SERVER player direction is updated
        m_syncThisFrame.DirectionVector = PlayerDirection(lookVector_x, lookVector_y);

        // Then, SERVER player movement is set in motion
        m_syncThisFrame.CharacterVelocity = PlayerMove(moveVector_x, moveVector_y, didJumpThisFrame);

        // With that, CLIENT player position is updated
        m_syncThisFrame.Position = transform.position;
        UpdatePlayerPositionClientRPC(m_syncThisFrame);
    }

    // This function updates the movement in "Overhead View"
    // this will ONLY take in movement vector and Current control state

    // This function updates the zoom of the overhead camera
    [ServerRpc]
    public void OverheadViewZoomUpdateServerRpc(ushort zoomAmount) {

    }

    // Once the movement for this Update has been processed, sends it back to the client to confirm their movements
    [ClientRpc]
    private void UpdatePlayerPositionClientRPC(PositionSync positionSync) {
        if (!IsLocalPlayer)
        {
            GroundedCheck();
            PlayerDirection(positionSync.DirectionVector);
            PlayerMove(positionSync.Position);
        }
        else if (IsLocalPlayer)
            ReconcileMovementToServer(positionSync);
    }




    // This is the full PlayerDirection "simulation", run on both the SERVER/HOST and CLIENT
    private Vector2 PlayerDirection(float look_x, float look_y) {
        // NOTE
        // x = playerRotationY
        // y = cameraRotationX

        if (look_x != 0f)
        {
            // Horizontal Player Rotation
            // Rotates the transform with the Look vector around local Y-axis
            transform.Rotate(new Vector3(0f, look_x * m_lookSensitivity, 0f), Space.Self);
        }

        if (look_y != 0f)
        {
            // Vertical Camera Rotation
            // Adds the vertical component (y) of the Look vector to camera's vertical angle
            m_cameraVerticalAngle += -look_y * m_lookSensitivity;

            // Limit camera's verticle angle to -89/+89 to prvent errors
            m_cameraVerticalAngle = Mathf.Clamp(m_cameraVerticalAngle, -m_cameraClampAngle, m_cameraClampAngle);
        }

        return CheckDirectionVector();
    }

    // This will ONLY run on clients (except Host) to apply DIRECTION from simulation
    private void PlayerDirection(Vector2 rotationVector) {
        // NOTE
        // x = playerRotationY
        // y = cameraRotationX

        if (rotationVector.x != 0f)
        {
            // Horizontal Player Rotation
            // Rotates player to the new axis
            transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, rotationVector.x, transform.rotation.eulerAngles.z));
        }

        if (rotationVector.y != 0f)
        {
            // Vertical Camera Rotation
            // Sets the camera angle to what was set on the server already
            m_cameraVerticalAngle = rotationVector.y;
        }
    }

    // The angle is applied and the camera will grab the info when called for it
    public float GetVerticalCameraAngle() => m_cameraVerticalAngle;




    // This is the full PlayerMove "simulation", run on both the SERVER/HOST and CLIENT
    private Vector3 PlayerMove(float move_x, float move_z, bool didJumpThisFrame) {
        // Clear TargetVector
        TargetVector = Vector3.zero;

        // apply movement component to calculate movement vector
        CharacterVector = new Vector3(move_x, 0.0f, move_z);

        // clamp to max magnitude of 1, otherwise diagonal movement might exceed max movement speed defined
        CharacterVector = Vector3.ClampMagnitude(CharacterVector, 1);
        //if ((move_x + move_z) == 2)
        //    Debug.LogWarning("PlayerMovement: Better return the ClampMagnitude! Lazy dog, bring it back >:C");

        // convers move input to a worldspace vector based on character's transform orientation
        Vector3 worldspaceMoveInput = transform.TransformVector(CharacterVector);

        if (IsGrounded)
        {
            // CURVES LATER HERE

            // calculated player's velocity from inputs and max speed allowed
            TargetVector = worldspaceMoveInput * m_maxSpeedOnGround/* * speedModifier*/;

            // Smoothly Interpolate between current velocity and target velocity
            CharacterVelocity = Vector3.Lerp(CharacterVelocity, TargetVector, m_movementSharpnessOnGround * Time.deltaTime);

            if ((0.0001f < CharacterVelocity.x) && CharacterVelocity.x < -0.0001f) CharacterVelocity.x = 0;
            if ((0.0001f < CharacterVelocity.y) && CharacterVelocity.y < -0.0001f) CharacterVelocity.y = 0;
            if ((0.0001f < CharacterVelocity.z) && CharacterVelocity.z < -0.0001f) CharacterVelocity.z = 0;

            if (didJumpThisFrame)
            {
                // ADD JumpForce to Character Velocity for (instant impulse)
                CharacterVelocity = new Vector3(CharacterVelocity.x, m_jumpForce, CharacterVelocity.z);

                // All the variables of other systems updated
                m_lastTimeJumped = Time.time;
                if (IsServer)
                    IsGrounded = false;
                m_GroundNormal = Vector3.up;
            }
        }
        else // Air logic
        {
            // Allow in-air movement?
            CharacterVelocity += worldspaceMoveInput * m_maxSpeedInAir * Time.deltaTime;

            // Add limit for maximum air movements
            float verticalVelocity = CharacterVelocity.y;
            Vector3 horizontalVelocity = Vector3.ProjectOnPlane(CharacterVelocity, Vector3.up);
            horizontalVelocity = Vector3.ClampMagnitude(horizontalVelocity, m_maxSpeedInAir);
            CharacterVelocity = horizontalVelocity + (Vector3.up * verticalVelocity);

            // apply the gravity to the velocity
            CharacterVelocity += Vector3.down * m_gravityY * Time.deltaTime;
        }

        Vector3 capsuleBottomBeforeMove = GetCapsuleBottomHemisphere();
        Vector3 capsuleTopBeforeMove = GetCapsuleTopHemisphere(m_characterController.height);
        // Moves player in the desired direction
        m_characterController.Move(CharacterVelocity * Time.deltaTime);

        // detect obstructions to adjust velocity accordingly
        //m_LatestImpactSpeed = Vector3.zero;
        if (Physics.CapsuleCast(capsuleBottomBeforeMove, capsuleTopBeforeMove, m_characterController.radius,
            CharacterVelocity.normalized, out RaycastHit hit, CharacterVelocity.magnitude * Time.deltaTime, -1,
            QueryTriggerInteraction.Ignore))
        {
            // We remember the last impact speed because the fall damage logic might need it
            //  m_LatestImpactSpeed = CharacterVelocity;

            CharacterVelocity = Vector3.ProjectOnPlane(CharacterVelocity, hit.normal);
        }

        return CharacterVelocity;
    }

    // This will ONLY run on clients (except Host) to apply MOVEMENT from simulation
    private void PlayerMove(Vector3 _position) {
        m_characterController.Move((_position - transform.position) * Time.deltaTime);
    }




    // This runs ONLY on clients (called via PlayerControls for LOCAL movement)
    public void ClientMovementPrediction(Vector2 moveVector, Vector2 lookVector, bool didJumpThisFrame, PlayerControls.ControlStates currentControlState) {
        PositionSync pos = new PositionSync();
        pos.Time = NetworkManager.ServerTime.Time;
        pos.CurrentControlState = currentControlState;

        // Grounded Check
        pos.IsGrounded = GroundedCheck();

        // Direction
        pos.DirectionVector = PlayerDirection(lookVector.x, lookVector.y);

        // Move
        pos.CharacterVelocity = PlayerMove(moveVector.x, moveVector.y, didJumpThisFrame);
        //Debug.Log("Character Velocity during MovementPrediction: " + CharacterVelocity);

        pos.Position = transform.position;
        m_previousPositions.Add(pos);
    }

    // This runs ONLY on clients (called via SERVER to CLIENTS, updates the player's position)
    // Only runs on LOCAL Clients, this is the server sending back confirmation of input
    private void ReconcileMovementToServer(PositionSync syncData) {
        Vector3 _position = syncData.Position;
        Vector2 _direction = syncData.DirectionVector;
        Vector3 _velocity = syncData.CharacterVelocity;

        //Debug.LogWarning("Reconciling to Server Sync");

        // Now that we have server confirmed sync data, we go BACK to look at local simulation to when the data matches
        for (int i = 0; i < m_previousPositions.Count; i++)
        {
            // Checking if the Character Velocity also matches up can be a good way to check if they've cheated??? idk
            //if (syncData.CharacterVelocity == m_previousPositions[i].CharacterVelocity)
            //    Debug.Log("Server calculated Character Velocity is the same as local simulated Character Velocity.");
            //Debug.Log("@(" + syncData.Time + ") SyncData Character Velocity: " + syncData.CharacterVelocity);
            //Debug.Log("@(" + m_previousPositions[i].Time + ") Local Simulated Character Velocity" + m_previousPositions[i].CharacterVelocity);

            if (_position == m_previousPositions[i].Position &&
                _direction == m_previousPositions[i].DirectionVector &&
                _velocity == m_previousPositions[i].CharacterVelocity)
            {
                m_previousPositions.RemoveRange(0, i);
                return;
            }
        }

        //Debug.LogWarning("Unable to find any match... Reconciling with server");

        m_previousPositions.Clear();

        IsGrounded = syncData.IsGrounded;
        CharacterVelocity = _velocity;
        PlayerDirection(_direction);
        PlayerMove(_position);
        
        //Debug.Log("Finished enacting on server data");
    }

    Vector2 CheckDirectionVector() {
        return new Vector2(transform.rotation.eulerAngles.y, m_cameraVerticalAngle);
    }
}
