using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterModelsManager : MonoBehaviour
{
    // Maybe remove?
    // Unsure of the uses as CharacterDataManager could be the one to handle this logic instead
    
    public static CharacterModelsManager Instance => _instance;
    private static CharacterModelsManager _instance;

    public List<GameObject> CharacterModels;

    private void Awake() {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(this);
    }
}
