using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

// This will turn into a networking script
// Managing localPlayer and their Client variants
// Plus how the server will interact with the script

public class CharacterDataManager : MonoBehaviour
{
    public static CharacterDataManager Instance => _instance;
    private static CharacterDataManager _instance;

    // temp solution until character data can be saved as a standalone asset
    //public List<GameObject> CharactersList = new List<GameObject>();
    [SerializeField] private List<GameObject> _characterNetworkedModels = new List<GameObject>();
    [SerializeField] private List<GameObject> _characterLocalModels = new List<GameObject>();

    public List<CatCharacterData> CatCharacterList = new List<CatCharacterData>();
    public List<MouseCharacterData> MouseCharacterList = new List<MouseCharacterData>();

    private void Awake() {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(this);

        // CONSTUCTOR, REMEMBER YOUR MONOGAME DAYS
        // This function will ADD ALL of the character data needed
        SetUpCharacters();

        //if (CharactersList.Count > 0)
        //    foreach (var gO in CharactersList)
        //    {
        //        if (gO.TryGetComponent(out CatCharacterData catData))
        //            CatCharacterList.Add(catData);
        //        if (gO.TryGetComponent(out MouseCharacterData mouseData))
        //            MouseCharacterList.Add(mouseData);
        //    }
        //else
        //{
        //    Debug.LogError("CharacterDataManager: No character data available", this);
        //    return;
        //}
    }

    private void Start() {
        //NetworkManager.Singleton.OnServerStarted += OnCharacterJoin;
    }

    // this is called when joining server
    private void OnCharacterJoin() {

    }


    private void SetUpCharacters() {
        SetUpCatCharacters();
        SetUpMouseCharacters();
    }

    private void SetUpCatCharacters() {
        // Cat Character 1
        CatCharacterList.Add(new CatCharacterData(32115, "Cat 1", "meow", CharacterDataBase.CharacterTypes.Cat, _characterNetworkedModels[0], null));

        // Cat Character 2
        CatCharacterList.Add(new CatCharacterData(21535, "Cat 2", "MOW!!!", CharacterDataBase.CharacterTypes.Cat, _characterNetworkedModels[1], null));

        // Cat Character 3
        
    }

    private void SetUpMouseCharacters() {
        // Mouse Character 1
        MouseCharacterList.Add(new MouseCharacterData(56415, "Mouse 1", "squeak", CharacterDataBase.CharacterTypes.Mouse, _characterNetworkedModels[2], null));

        // Mouse Character 2
        MouseCharacterList.Add(new MouseCharacterData(21354, "Mouse 2", "SQUEAK!!!", CharacterDataBase.CharacterTypes.Mouse, _characterNetworkedModels[3], null));

        // Mouse Character 3
        MouseCharacterList.Add(new MouseCharacterData(12343, "Mouse 3", "wow!!!", CharacterDataBase.CharacterTypes.Mouse, _characterNetworkedModels[4], null));

        // Mouse Character 4

        // Mouse Character 5

    }
}
