using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MouseCharacterData : CharacterDataBase
{
    public override void Initialise() {
        base.Initialise();
    }

    public MouseCharacterData(ushort mouse_uuid, string mouse_name, string mouse_description, CharacterTypes mouse_charType, GameObject mouse_gO, GameObject mouse_localGO) {
        _uuid = mouse_uuid;
        _name = mouse_name;
        _description = mouse_description;
        _characterType = mouse_charType;
        _characterNetworkedModel = mouse_gO;
        _characterLocalModel = mouse_localGO;

        Initialise();
    }
}
