using System;
using System.Collections.Generic;
using UnityEngine;

// REWRITE THIS
// NO NEED FOR SEPARATE CLASSES FOR DIFFERENT CHARACTERS

[Serializable]
public abstract class CharacterDataBase
{
    public enum CharacterTypes
    {
        Unselected,
        SPECTATOR,
        Cat,
        Mouse
    }

    public ushort UUID => _uuid;
    [SerializeField] protected ushort _uuid;

    // Basic information about the character
    public string Name => _name;
    [SerializeField] protected string _name;
    public string Description => _description;
    [SerializeField] protected string _description;
   
    // Character type
    public CharacterTypes CharacterType => _characterType;
    [SerializeField] protected CharacterTypes _characterType;

    // Character models
    public GameObject CharacterNetworkedModel => _characterNetworkedModel;
    [SerializeField] protected GameObject _characterNetworkedModel;
    public GameObject CharacterLocalModel => _characterLocalModel;
    [SerializeField] protected GameObject _characterLocalModel;

    // Character functions
    // Called when the player loads into the game
    public virtual void Initialise() {
        _name = (_name == "") || (_name == null) ? "Default Character" : _name;
        _description = (_description == "") || (_description == null) ? "*insert description here*" : _description;
        _characterType = (_characterType == CharacterTypes.Unselected) ? CharacterTypes.SPECTATOR : _characterType;

        Debug.Log("CharacterData for " + _name + " initilaised.");
    }

    // Called when the player JOINS the game
    public virtual void SetUp() {
        Debug.LogWarning("Nothing in set-up code yet");
        throw new NotImplementedException();
    }

    // Other functions to run on all characters?
}
