using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CatCharacterData : CharacterDataBase
{
    public override void Initialise() {
        base.Initialise();
    }

    public void Meow() {
        Debug.Log("Meow");
    }

    // TODO: Re-write Cat/Mouse Data to add data via Constructors

    public CatCharacterData(ushort cat_uuid, string cat_name, string cat_description, CharacterTypes cat_charType, GameObject cat_gO, GameObject cat_localGO) {
        _uuid = cat_uuid;
        _name = cat_name;
        _description = cat_description;
        _characterType = cat_charType;
        _characterNetworkedModel = cat_gO;
        _characterLocalModel = cat_localGO;

        Initialise();
    }
}
