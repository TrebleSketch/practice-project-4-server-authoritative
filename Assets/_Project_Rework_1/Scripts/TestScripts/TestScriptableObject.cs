using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TestSOCharacterFile", menuName = "ScriptableObjects/TestCharacterFile", order = 1)]
public class TestScriptableObject : ScriptableObject
{
    public short UUID;

    public string Name;
    public string Description;
    public int IntTester;
    public Vector3[] Vector3Array;

    public GameObject GOAttached;
}
