﻿using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectProcessingTest : MonoBehaviour
{
    public List<TestScriptableObject> testScriptableObjects = new List<TestScriptableObject>();

    void Start() {
        OutputData();
    }
    
    void OutputData() {
        foreach (var scriptObject in testScriptableObjects)
        {
            Debug.Log(
                "========================================================\n" +
                "Test SO\n" +
                "========================================================");

            Debug.Log("UUID: " + scriptObject.UUID);
            Debug.Log("Name: " + scriptObject.Name);
            Debug.Log("Description: " + scriptObject.Description);
            Debug.Log("Int tester: " + scriptObject.IntTester);

            Debug.Log("==> Vector 3 test array <==");
            for (int i = 0; i < scriptObject.Vector3Array.Length; i++)
            {
                Debug.LogFormat("Vector 3 #{0}: {1}", i, scriptObject.Vector3Array[i]);
            }

            Debug.Log("Gameobject Attached's name: " + scriptObject.GOAttached.name);
            GameObject scriptSpawnedGO = Instantiate(scriptObject.GOAttached, scriptObject.Vector3Array[0], Quaternion.identity);
            scriptSpawnedGO.name = scriptObject.GOAttached.name;
        }
    }
}