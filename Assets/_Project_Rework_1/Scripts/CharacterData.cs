﻿using UnityEngine;

namespace CAM
{
    public enum CharacterTypes
    {
        Unselected,
        SPECTATOR,
        Cat,
        Mouse
    }

    [CreateAssetMenu(fileName = "CharacterData", menuName = "CharacterDataTemplate", order = 0)]
    public class CharacterData : ScriptableObject
    {
        public ushort UUID => _uuid;
        [SerializeField] protected ushort _uuid = 0;

        // Basic information about the character
        public string Name => _name;
        [SerializeField] protected string _name = "Example Name";
        public string Description => _description;
        [SerializeField] protected string _description = "Example Description";

        // Character type
        public CharacterTypes CharacterType => _characterType;
        [SerializeField] protected CharacterTypes _characterType;

        // Character models
        public GameObject CharacterNetworkedModel => _characterNetworkedModel;
        [SerializeField] protected GameObject _characterNetworkedModel;
        public GameObject CharacterLocalModel => _characterLocalModel;
        [SerializeField] protected GameObject _characterLocalModel;
    }
}